import { error } from '@sveltejs/kit';

export async function load({ params, parent }) {
  const { videos } = await parent();
  const { id } = params;

  const video = videos.find(video => video.id === id);

  if (!video) throw error(404);

  return {
    video
  };
}
